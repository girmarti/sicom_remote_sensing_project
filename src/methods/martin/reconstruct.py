"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
import cv2

from scipy.ndimage import convolve, gaussian_filter
from scipy.signal import medfilt2d


## preprocessing

def upsample_bicubic_ms(pan, ms):
    

    pan_h, pan_w = pan.shape

    up_sampled_image = cv2.resize(ms, (pan_w,pan_h), interpolation=cv2.INTER_CUBIC)
    
    return up_sampled_image

def norm_std(x, m, s):
    """Normalize an array in order to have a given mean and std"""
    
    x_norm = s * (x - x.mean(axis=(0,1))) / x.std(axis=(0,1)) + m

    return x_norm


def norm_std_img(x, y):
    """Normalize an image x in order to have a given mean and std from image y"""

    if x.ndim > 2:
        x_norm = x
        for i in range(x.shape[2]):
            x_norm[:,:,i] = norm_std_img(x[:,:,i], y[:,:,i])
        return x_norm
    else:
        y_mean = y.mean(axis=(0,1))
        y_std = y.std(axis=(0,1))
    
        # print(y_mean, y_std)

        x_norm = norm_std(x, y_mean, y_std)
        return x_norm
    
def computing_I_mat(ms_image):

    i_mat = 0


    for band in range(ms_image.shape[-1]):
        i_mat += 1/len(ms_image[0,0])*ms_image[:,:,band]

    return i_mat


def preprocessing_reconstruction(pan: np.ndarray,  ms: np.ndarray) -> np.ndarray:
    """Performs ata fusion of the three acquisitions.

    Args:
        pan (np.ndarray): Panchromatic image.
        ms (np.ndarray): Low resolution multispectral image.

    Returns:
        np.ndarray: Full spatial and spectral iamge.
    """


    ms_upsampled = upsample_bicubic_ms(pan, ms)

    i_l = computing_I_mat(ms_upsampled)

    pan_normed = norm_std_img(pan, i_l)

    return pan_normed, ms_upsampled, i_l

    
## CS general scheme

def component_substitution_generalized(pan, ms_image, i_mat):

    new_img = np.zeros(np.shape(ms_image))
    g = 1

    for band in range(np.shape(ms_image)[-1]):
        new_img[:,:,band] = ms_image[:,:,band] + g*(pan - i_mat)

    return new_img

## CS multiplicative

def component_substitution_multiplicative(p_mat, ms_img, i_mat):

    new_img = np.zeros(np.shape(ms_img))


    for band in range(np.shape(ms_img)[2]):
        new_img[:,:,band] = np.multiply(ms_img[:,:,band],p_mat/i_mat)

    return new_img

## Low pass filtering CS general scheme


def low_pass_mean_CS_general_scheme(p_mat, ms_img, size_kernel):

    kernel = np.ones((size_kernel, size_kernel)) / (size_kernel**2)
    P_filtre = convolve(p_mat, kernel)

    g = 1

    new_img = np.zeros(np.shape(ms_img))
    for band in range(np.shape(ms_img)[2]):
        new_img[:,:,band] = ms_img[:,:,band] + g*(p_mat - P_filtre)
    return new_img


def low_pass_med_CS_general_scheme(p_mat, ms_img, size_kernel):

    P_fil_med = medfilt2d(p_mat.astype(np.float32), size_kernel)

    g = 1

    new_img = np.zeros(np.shape(ms_img))
    for band in range(np.shape(ms_img)[2]):
        new_img[:,:,band] = ms_img[:,:,band] + g*(p_mat - P_fil_med)
    return new_img


def low_pass_gaussian_CS_general_scheme(p_mat, ms_img, sigma=1):

    P_fil_gauss = gaussian_filter(p_mat.astype(np.float32),sigma)
    g = 1

    new_img = np.zeros(np.shape(ms_img))
    for band in range(np.shape(ms_img)[2]):
        new_img[:,:,band] = ms_img[:,:,band] + g*(p_mat - P_fil_gauss)
    return new_img


## Low pass filtering CS multiplicative scheme

def low_pass_mean_CS_multiplicative_scheme(p_mat, ms_img, i_mat, size_kernel):

    kernel = np.ones((size_kernel, size_kernel)) / (size_kernel**2)
    P_filtre = convolve(p_mat, kernel)

    new_img = np.zeros(np.shape(ms_img))

    for band in range(np.shape(ms_img)[2]):
        new_img[:,:,band] = np.multiply(ms_img[:,:,band],P_filtre/i_mat)

    return new_img


def low_pass_med_CS_multiplicative_scheme(p_mat, ms_img, i_mat, size_kernel):

    P_fil_med = medfilt2d(p_mat.astype(np.float32), size_kernel)

    new_img = np.zeros(np.shape(ms_img))

    for band in range(np.shape(ms_img)[2]):
        new_img[:,:,band] = np.multiply(ms_img[:,:,band],P_fil_med/i_mat)
    return new_img


def low_pass_gaussian_CS_multiplicative_scheme(p_mat, ms_img, i_mat, sigma=1):

    P_fil_gauss = gaussian_filter(p_mat.astype(np.float32),sigma)

    new_img = np.zeros(np.shape(ms_img))

    for band in range(np.shape(ms_img)[2]):
        new_img[:,:,band] = np.multiply(ms_img[:,:,band],P_fil_gauss/i_mat)
    return new_img





####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
